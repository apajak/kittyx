from crypto_history_manager import CryptoHistoryData
from mysql.connector import errorcode
import datetime
import mysql.connector


class DatabaseManager:
    def __init__(self, host, user, password, database):
        self.host = host
        self.user = user
        self.password = password
        self.database = database
        self.cnx = None
        self.cursor = None

    def connect(self):
        try:
            self.cnx = mysql.connector.connect(host=self.host,
                                               user=self.user,
                                               password=self.password,
                                               database=self.database,
                                               autocommit=True)
            self.cursor = self.cnx.cursor()
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)

    def close(self):
        self.cursor.close()
        self.cnx.close()

    def execute(self, query, args=None):
        if args is None:
            self.cursor.execute(query)
        else:
            self.cursor.execute(query, args)

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()

    def get_last_row_id(self):
        return self.cursor.lastrowid

    def get_row_count(self):
        return len(self.fetchall())
## -----------------------------------------crypto_history_data---------------------------------------------------------
    ### TABLE crypto_history_data ###
    # TABLE crypto_history_data (
    #     id INT AUTO_INCREMENT PRIMARY KEY,
    #     unix_timestamp BIGINT,
    #     date DATE,
    #     hour INT,
    #     symbol VARCHAR(255),
    #     type VARCHAR(255),
    #     strike VARCHAR(255),
    #     open DECIMAL(10, 4),
    #     high DECIMAL(10, 4),
    #     low DECIMAL(10, 4),
    #     close DECIMAL(10, 4),
    #     volume_in DECIMAL(10, 4),
    #     volume_in_symbol VARCHAR(255),
    #     volume_out DECIMAL(10, 4),
    #     volume_out_symbol VARCHAR(255),
    #     tradecount INT,
    #     best_bid_price DECIMAL(10, 4),
    #     best_ask_price DECIMAL(10, 4),
    #     best_bid_qty DECIMAL(10, 4),
    #     best_ask_qty DECIMAL(10, 4),
    #     best_buy_iv DECIMAL(10, 4),
    #     best_sell_iv DECIMAL(10, 4),
    #     mark_price DECIMAL(10, 4),
    #     mark_iv DECIMAL(10, 4),
    #     delta DECIMAL(10, 4),
    #     gamma DECIMAL(10, 4),
    #     vega DECIMAL(10, 4),
    #     theta DECIMAL(10, 4),
    #     openinterest_in DECIMAL(10, 4),
    #     openinterest_out DECIMAL(10, 4),
    #     provider VARCHAR(255)
    # );
    ### END TABLE crypto_history_data ###

    def insert_crypto_history_data(self, data):
        self.connect()
        query = ("INSERT INTO crypto_history_data "
                 "(unix_timestamp, date, hour, symbol, type, strike, open, high, low, close, volume_in, volume_in_symbol, "
                 "volume_out, volume_out_symbol, tradecount, best_bid_price, best_ask_price, best_bid_qty, best_ask_qty, "
                 "best_buy_iv, best_sell_iv, mark_price, mark_iv, delta, gamma, vega, theta, openinterest_in, "
                 "openinterest_out, provider, buyTakerAmount, buyTakerQuantity, weightedAverage) "
                 "VALUES (%(unix_timestamp)s, %(date)s, %(hour)s, %(symbol)s, %(type)s, %(strike)s, %(open)s, %(high)s, "
                 "%(low)s, %(close)s, %(volume_in)s, %(volume_in_symbol)s, %(volume_out)s, %(volume_out_symbol)s, "
                 "%(tradecount)s, %(best_bid_price)s, %(best_ask_price)s, %(best_bid_qty)s, %(best_ask_qty)s, "
                 "%(best_buy_iv)s, %(best_sell_iv)s, %(mark_price)s, %(mark_iv)s, %(delta)s, %(gamma)s, %(vega)s, "
                 "%(theta)s, %(openinterest_in)s, %(openinterest_out)s, %(provider)s, %(buyTakerAmount)s, "
                 "%(buyTakerQuantity)s, %(weightedAverage)s)")

        self.execute(query, data.toDict())
        id = self.get_last_row_id()
        self.close()
        return id

    def load_data_file(self, file_path):
        self.connect()
        query = ("LOAD DATA INFILE %s INTO TABLE crypto_history_data "
                 "FIELDS TERMINATED BY ',' "
                 "LINES TERMINATED BY '\n' "
                 "(unix_timestamp, date, hour, symbol, type, strike, open, high, low, close, volume_in, volume_in_symbol, "
                 "volume_out, volume_out_symbol, tradecount, best_bid_price, best_ask_price, best_bid_qty, best_ask_qty, "
                 "best_buy_iv, best_sell_iv, mark_price, mark_iv, delta, gamma, vega, theta, openinterest_in, "
                 "openinterest_out, provider, buyTakerAmount, buyTakerQuantity, weightedAverage)")
        self.execute(query, (file_path,))
        self.close()

    def get_cryto_history_data_by_symbol(self, symbol):
        self.connect()
        query = ("SELECT * FROM crypto_history_data WHERE symbol = %s")
        self.execute(query, (symbol,))
        historys = [CryptoHistoryData.fromTuple(history_tuple) for history_tuple in self.fetchall()]
        return historys

    def get_crypto_history_by_out_symbol(self, symbol):
        self.connect()
        # get all crypto_history_data where volume_out_symbol contain '/symbol'
        query = ("SELECT * FROM crypto_history_data WHERE volume_out_symbol LIKE %s")
        self.execute(query, ('%' + symbol + '%',))
        historys = [CryptoHistoryData.fromTuple(history_tuple) for history_tuple in self.fetchall()]
        return historys

    def get_symbol_list(self):
        self.connect()
        query = ("SELECT DISTINCT symbol FROM crypto_history_data")
        self.execute(query)
        symbols = [symbol_tuple[0] for symbol_tuple in self.fetchall()]
        self.close
        return symbols

    def update_symbol(self, old_symbol, new_symbol):
        self.connect()
        query = ("UPDATE crypto_history_data SET symbol = %s WHERE symbol = %s")
        self.execute(query, (new_symbol, old_symbol))
        self.close()

## ---------------------------------------crypto_history_data END-------------------------------------------------------