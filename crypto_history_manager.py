class CryptoHistoryData:
    def __init__(self, id=None, unix_timestamp=None, date=None, hour=0, symbol=None, type=None, strike=None,
                 open_val=0.0000, high=0.0000, low=0.0000, close=0.0000, volume_in=0.0000, volume_in_symbol=None,
                 volume_out=0.0000, volume_out_symbol=None, tradecount=0, best_bid_price=0.0000,
                 best_ask_price=0.0000, best_bid_qty=0.0000, best_ask_qty=0.0000, best_buy_iv=0.0000,
                 best_sell_iv=0.0000, mark_price=0.0000, mark_iv=0.0000, delta=0.0000, gamma=0.0000, vega=0.0000,
                 theta=0.0000, openinterest_in=0.0000, openinterest_out=0.0000, provider=None,buyTakerAmount=0.0000,
                 buyTakerQuantity=0.0000,weightedAverage=0.0000):
        self.id = id
        self.unix_timestamp = unix_timestamp
        self.date = date
        self.hour = hour
        self.symbol = symbol
        self.type = type
        self.strike = strike
        self.open_val = round(float(open_val), 4)
        self.high = round(float(high), 4)
        self.low = round(float(low), 4)
        self.close = round(float(close), 4)
        self.volume_in = round(float(volume_in), 4)
        self.volume_in_symbol = volume_in_symbol
        self.volume_out = round(float(volume_out), 4)
        self.volume_out_symbol = volume_out_symbol
        self.tradecount = tradecount
        self.best_bid_price = round(float(best_bid_price), 4)
        self.best_ask_price = round(float(best_ask_price), 4)
        self.best_bid_qty = round(float(best_bid_qty), 4)
        self.best_ask_qty = round(float(best_ask_qty), 4)
        self.best_buy_iv = round(float(best_buy_iv), 4)
        self.best_sell_iv = round(float(best_sell_iv), 4)
        self.mark_price = round(float(mark_price), 4)
        self.mark_iv = round(float(mark_iv), 4)
        self.delta = round(float(delta), 4)
        self.gamma = round(float(gamma), 4)
        self.vega = round(float(vega), 4)
        self.theta = round(float(theta), 4)
        self.openinterest_in = round(float(openinterest_in), 4)
        self.openinterest_out = round(float(openinterest_out), 4)
        self.provider = provider
        self.buyTakerAmount = round(float(buyTakerAmount), 4)
        self.buyTakerQuantity = round(float(buyTakerQuantity), 4)  
        self.weightedAverage = round(float(weightedAverage), 4)


    def __str__(self):
        return (f"{self.id}, {self.unix_timestamp}, {self.date}, {self.hour}, {self.symbol}, {self.type}, {self.strike}"
                f", {self.open_val}, {self.high}, {self.low}, {self.close}, {self.volume_in}, {self.volume_in_symbol},"
                f" {self.volume_out}, {self.volume_out_symbol}, {self.tradecount}, {self.best_bid_price},"
                f" {self.best_ask_price}, {self.best_bid_qty}, {self.best_ask_qty}, {self.best_buy_iv},"
                f" {self.best_sell_iv}, {self.mark_price}, {self.mark_iv}, {self.delta}, {self.gamma}, {self.vega},"
                f" {self.theta}, {self.openinterest_in}, {self.openinterest_out}, {self.provider},"
                f" {self.buyTakerAmount}, {self.buyTakerQuantity}, {self.weightedAverage}")

    def toTuple(self):
        return (self.id, self.unix_timestamp, self.date, self.hour, self.symbol, self.type, self.strike,
                self.open_val, self.high, self.low, self.close, self.volume_in, self.volume_in_symbol,
                self.volume_out, self.volume_out_symbol, self.tradecount, self.best_bid_price,
                self.best_ask_price, self.best_bid_qty, self.best_ask_qty, self.best_buy_iv,
                self.best_sell_iv, self.mark_price, self.mark_iv, self.delta, self.gamma, self.vega,
                self.theta, self.openinterest_in, self.openinterest_out, self.provider, self.buyTakerAmount,
                self.buyTakerQuantity, self.weightedAverage)

    def toDict(self):
        return {
            "id": self.id,
            "unix_timestamp": self.unix_timestamp,
            "date": self.date,
            "hour": self.hour,
            "symbol": self.symbol,
            "type": self.type,
            "strike": self.strike,
            "open": self.open_val,
            "high": self.high,
            "low": self.low,
            "close": self.close,
            "volume_in": self.volume_in,
            "volume_in_symbol": self.volume_in_symbol,
            "volume_out": self.volume_out,
            "volume_out_symbol": self.volume_out_symbol,
            "tradecount": self.tradecount,
            "best_bid_price": self.best_bid_price,
            "best_ask_price": self.best_ask_price,
            "best_bid_qty": self.best_bid_qty,
            "best_ask_qty": self.best_ask_qty,
            "best_buy_iv": self.best_buy_iv,
            "best_sell_iv": self.best_sell_iv,
            "mark_price": self.mark_price,
            "mark_iv": self.mark_iv,
            "delta": self.delta,
            "gamma": self.gamma,
            "vega": self.vega,
            "theta": self.theta,
            "openinterest_in": self.openinterest_in,
            "openinterest_out": self.openinterest_out,
            "provider": self.provider,
            "buyTakerAmount": self.buyTakerAmount,
            "buyTakerQuantity": self.buyTakerQuantity,
            "weightedAverage": self.weightedAverage
        }

def fromTuple(self, tuple):
    self.id = tuple[0]
    self.unix_timestamp = tuple[1]
    self.date = tuple[2]
    self.hour = tuple[3]
    self.symbol = tuple[4]
    self.type = tuple[5]
    self.strike = tuple[6]
    self.open_val = tuple[7]
    self.high = tuple[8]
    self.low = tuple[9]
    self.close = tuple[10]
    self.volume_in = tuple[11]
    self.volume_in_symbol = tuple[12]
    self.volume_out = tuple[13]
    self.volume_out_symbol = tuple[14]
    self.tradecount = tuple[15]
    self.best_bid_price = tuple[16]
    self.best_ask_price = tuple[17]
    self.best_bid_qty = tuple[18]
    self.best_ask_qty = tuple[19]
    self.best_buy_iv = tuple[20]
    self.best_sell_iv = tuple[21]
    self.mark_price = tuple[22]
    self.mark_iv = tuple[23]
    self.delta = tuple[24]
    self.gamma = tuple[25]
    self.vega = tuple[26]
    self.theta = tuple[27]
    self.openinterest_in = tuple[28]
    self.openinterest_out = tuple[29]
    self.provider = tuple[30]
    self.buyTakerAmount = tuple[31]
    self.buyTakerQuantity = tuple[32]
    self.weightedAverage = tuple[33]
    return self
