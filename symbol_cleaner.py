from database_manager import DatabaseManager
from dotenv import load_dotenv
import os
import json
import time
from tqdm import tqdm
import sys
import threading

load_dotenv(".env")
db_host = os.getenv("DATABASE_HOST")
db_user = os.getenv("DATABASE_USER")
db_password = os.getenv("DATABASE_PASSWORD")
db_name = os.getenv("DATABASE_NAME")
db = DatabaseManager(db_host, db_user, db_password, db_name)
db.connect()


# Load the list of symbols from the database
# print("Loading symbol list from database...")
# db_symbol_list = db.get_symbol_list()
# print(f"db_symbol_list: {len(db_symbol_list)} \n")



print("Loading symbol list from file... ")
try:
    # Load the list of compliant symbols
    with open('symbol_list.json', 'r') as symbol_list_file:
        symbol_list = json.load(symbol_list_file)
        print(f"symbol_list: {len(symbol_list)} \n")
except FileNotFoundError:
    print("Error: File 'symbol_list.json' not found. \n")
    sys.exit(1)
except json.JSONDecodeError:
    print("Error: Unable to parse JSON file 'symbol_list.json'. \n")
    sys.exit(1)

compliant_symbols = []
non_compliant_symbols = []
date = time.strftime('%Y-%m-%d', time.localtime(int(time.time())))

# print("Checking for non compliant symbols... ")
# for symbol in db_symbol_list:
# # if can be split into two symbols, add to compliant_symbols
#     if '/' in symbol:
#         if symbol.split('/')[0] not in symbol_list:
#             symbol_list.append(symbol.split('/')[0])
#         if symbol.split('/')[1] not in symbol_list:
#             symbol_list.append(symbol.split('/')[1])
#     else :
#         non_compliant_symbols.append(symbol)


# Load the list of symbols from the file
print("Loading symbol list from file... ")
try:
    # Load the list of compliant symbols
    with open('non_compliant_symbols.json', 'r') as symbol_list_file:
        non_compliant_symbols = json.load(symbol_list_file)
        print(f"non_compliant_symbols: {len(non_compliant_symbols)} \n")
except FileNotFoundError:
    print("Error: File 'non_compliant_symbols.json' not found. \n")
    sys.exit(1)

print("Reformat non compliant symbols... ")
# Initialize lists to store compliant and invalid symbols
compliant_symbols = {}
invalid_symbols = []

# Iterate through non-compliant symbols
for non_compliant_symbol in non_compliant_symbols:
    old_symbol = non_compliant_symbol
    symbol_matches = [symbol for symbol in symbol_list if symbol in non_compliant_symbol]

    if not symbol_matches:
        invalid_symbols.append(old_symbol)
    else:
        one_match = False
        for symbol_match in symbol_matches:
            if non_compliant_symbol.startswith(symbol_match):
                one_match = True
                start_symbol = symbol_match
                end_symbol = non_compliant_symbol.replace(symbol_match, '')
                if not end_symbol:
                    compliant_symbols[old_symbol] = start_symbol
                if end_symbol in symbol_matches:
                    new_symbol = f"{start_symbol}/{end_symbol}"
                    if new_symbol not in compliant_symbols:
                        compliant_symbols[old_symbol] = new_symbol
        if not one_match:
            invalid_symbols.append(old_symbol)

print("generate new symbols... ")
# Initialize a list to store new symbols
new_symbol_list = []
# Loop through invalid_symbols and check if they can be split into two symbols
for invalid_symbol in invalid_symbols:
    matches = [symbol for symbol in symbol_list if invalid_symbol.startswith(symbol) or invalid_symbol.endswith(symbol)]
    if len(matches) == 1:
        new_symbol = invalid_symbol.replace(matches[0], '')
        if new_symbol not in new_symbol_list:
            new_symbol_list.append(new_symbol)
    elif len(matches) > 1:
        # Keep the longest match
        longest_match = max(matches, key=len)
        new_symbol = invalid_symbol.replace(longest_match, '')
        if new_symbol not in new_symbol_list:
            new_symbol_list.append(new_symbol)

if new_symbol_list:
    print(f"generate file new_symbols_{date}.json... ")
    # Create a new JSON file for the new symbols to be added to the symbol_list after manual review
    with open(f'new_symbols_{date}.json', 'w') as new_symbols_file:
        json.dump(new_symbol_list, new_symbols_file)
    print(f"New symbols have been generated: {new_symbol_list}")

# Print the resulting data
print(f"new_symbol_list: {new_symbol_list}")
print(f"compliant_symbols ({len(compliant_symbols)}): {compliant_symbols}")
print(f"invalid_symbols ({len(invalid_symbols)}): {invalid_symbols}")

# create progress bar
print(f"Updating symbols in database...")
progress_bar = tqdm(total=len(compliant_symbols), desc="Updating symbols in database")
# # Update the symbols in the database
for old_symbol, new_symbol in compliant_symbols.items():
    db.update_symbol(old_symbol, new_symbol)
    progress_bar.update(1)
progress_bar.close()
print(f"Symbols have been updated in the database.")