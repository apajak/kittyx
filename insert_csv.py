import datetime

from dotenv import load_dotenv
import os
from database_manager import DatabaseManager
import csv
from crypto_history_manager import CryptoHistoryData
from tqdm import tqdm

load_dotenv(".env")
db_host = os.getenv("DATABASE_HOST")
db_user = os.getenv("DATABASE_USER")
db_password = os.getenv("DATABASE_PASSWORD")
db_name = os.getenv("DATABASE_NAME")
db = DatabaseManager(db_host, db_user, db_password, db_name)
db.connect()
data_csv_dir_path = "./csv"
data_folder = "./data/"
# Get all folders in data folder
folders = os.listdir(data_folder)
# Remove .py file
folders.remove("csv_extractor.py")
print(folders)
count = 0
data_count = 0
# Set the chunk size
chunk_size = 200000
chunk_count = 0
file_count = 0
start_time = datetime.datetime.now()
def create_csv_file(file_count, data_csv_dir_path):
    # Increment the file_count
    file_count += 1

    # Generate the CSV file path
    data_csv_path = os.path.join(data_csv_dir_path, f"crypto_history_data_{file_count}.csv")

    # Define the header as a single-line string without line breaks
    header = "unix_timestamp,date,hour,symbol,type,strike,open,high,low,close,volume_in,volume_in_symbol,volume_out," \
             "volume_out_symbol,tradecount,best_bid_price,best_ask_price,best_bid_qty,best_ask_qty,best_buy_iv," \
             "best_sell_iv,mark_price,mark_iv,delta,gamma,vega,theta,openinterest_in,openinterest_out,provider," \
             "buyTakerAmount,buyTakerQuantity,weightedAverage"

    # Write the header to the CSV file
    with open(data_csv_path, 'w', newline='') as data_csv:
        csv_writer = csv.writer(data_csv)
        csv_writer.writerow(header.split(','))  # Split the header string into a list

    return data_csv_path

# Create a total progress bar for all folders
print("Init progress...")
init_progress = tqdm(total=len(folders), desc="Init Progress", unit="folder")
for folder in folders:
    subfolders = os.listdir(os.path.join(data_folder, folder))
    subfolder_progress = tqdm(total=len(subfolders), desc=folder, unit="csv")
    for subfolder in subfolders:
        if folder == "Binance" and subfolder == "hour":
            continue
        else:
            csv_list = os.listdir(os.path.join(data_folder, folder, subfolder))
            for csv_file in csv_list:
                csv_file_path = os.path.join(data_folder, folder, subfolder, csv_file)
                # Open and read the CSV file
                with open(csv_file_path, 'r') as csv_file:
                    next(csv_file)
                    csv_reader = csv.reader(csv_file)
                    header = next(csv_reader)  # Get the header row
                    for row in csv_reader:
                        data_count += 1
        subfolder_progress.update(1)
    subfolder_progress.close()
    init_progress.update(1)
init_progress.close()
os.system('cls' if os.name == 'nt' else 'clear')
print("Init progress complete!")
print(f"Total data count: {data_count}")
# 31162150 rows
# create csv file
data_csv_path = create_csv_file(file_count, data_csv_dir_path)

total_progress = tqdm(total=data_count, desc="Total Progress", unit="csv")
for folder in folders:
    provider = folder.lower()
    # Get all subfolders in each folder
    subfolders = os.listdir(os.path.join(data_folder, folder))
    for subfolder in subfolders:
        # If folder is Binance and subfolder = hour then skip
        if folder == "Binance" and subfolder == "hour":
            csv_list = os.listdir(os.path.join(data_folder, folder, subfolder))
            for csv_file in csv_list:
                csv_file_path = os.path.join(data_folder, folder, subfolder, csv_file)
                # Open and read the CSV file
                with open(csv_file_path, 'r') as csv_file:
                    csv_reader = csv.reader(csv_file)
                    header = next(csv_reader)  # Get the header row
                    #date,hour,symbol,underlying,type,strike,open,high,low,close,volume_contracts,volume_usdt,best_bid_price,best_ask_price,best_bid_qty,best_ask_qty,best_buy_iv,best_sell_iv,mark_price,mark_iv,delta,gamma,vega,theta,openinterest_contracts,openinterest_usdt
                    for row in csv_reader:
                        date = row[0]
                        hour = row[1]
                        # if row[16] is string type
                        if type(row[16]) == str:
                            row[16] = 0.0000
                        # if row[24] is string type
                        if type(row[24]) == str:
                            row[24] = 0.0000
                        # if row[25] is string type
                        if type(row[25]) == str:
                            row[25] = 0.0000
                        convert_date = datetime.datetime.strptime(date, '%Y-%m-%d')
                        # add hour
                        convert_date = convert_date + datetime.timedelta(hours=int(hour))
                        unix_timestamp = int(datetime.datetime.timestamp(convert_date))
                        # BNBUSDT-EOHSummary-2023-05-24
                        # to BNB
                        file_name = csv_file_path.split('-')[0]
                        # symbole = filename minus USDT
                        symbol = file_name[:-4]
                        crypto_data = CryptoHistoryData(
                            unix_timestamp=unix_timestamp,
                            date=date,
                            hour=hour,
                            symbol=row[3],
                            type=row[2],
                            strike=row[5],
                            open_val=row[6],
                            high=row[7],
                            low=row[8],
                            close=row[9],
                            volume_in=row[10],
                            volume_in_symbol=symbol,
                            volume_out=row[11],
                            volume_out_symbol="USDT",
                            best_bid_price=row[12],
                            best_ask_price=row[13],
                            best_bid_qty=row[14],
                            best_ask_qty=row[15],
                            best_buy_iv=row[16],
                            best_sell_iv=row[17],
                            mark_price=row[18],
                            mark_iv=row[19],
                            delta=row[20],
                            gamma=row[21],
                            vega=row[22],
                            theta=row[23],
                            openinterest_in=row[24],
                            openinterest_out=row[25],
                            provider='binance'
                        )
                        # Insert into csv file
                        with open(data_csv_path, 'a', newline='') as data_csv:
                            csv_writer = csv.writer(data_csv)
                            tuple_without_id = crypto_data.toTuple()[1:]
                            # Write the tuple to the CSV file using csv_writer.writerow
                            csv_writer.writerow(tuple_without_id)
                            chunk_count += 1
                            if chunk_count == chunk_size:
                                chunk_count = 0
                                file_count += 1
                                data_csv_path = os.path.join(data_csv_dir_path, f"crypto_history_data_{file_count}.csv")
                        total_progress.update(1)
        else:
            # Get csv list in each subfolder
            csv_list = os.listdir(os.path.join(data_folder, folder, subfolder))

            for csv_file in csv_list:
                # Get csv file path
                csv_file_path = os.path.join(data_folder, folder, subfolder, csv_file)
                # Open and read the CSV file
                with open(csv_file_path, 'r') as csv_file:
                    next(csv_file)
                    csv_reader = csv.reader(csv_file)
                    header = next(csv_reader)  # Get the header row
                    # Extract column names and find the indices for volume_in and volume_out
                    try :
                        volume_in_label = header[7].split(' ')[1]  # Assuming Volume 1INCH is at index 7
                        volume_out_label = header[8].split(' ')[1]  # Assuming Volume BUSD is at index 8
                    except IndexError:
                        for row in csv_reader:
                            unix_timestamp, date, symbol, open_val, high, low, close, volume_in, volume_out = row[:9]
                            volume_in_label = symbol.split('/')[0]
                            volume_out_label = symbol.split('/')[1]
                            break
                    tradecount = 0
                    buyTakerAmount = 0
                    buyTakerQuantity = 0
                    weightAverage = 0
                    # get the number of rows in the csv file
                    for row in csv_reader:
                        count += 1
                        unix_timestamp, date, symbol, open_val, high, low, close, volume_in, volume_out = row[:9]
                        if len(header) == 10:
                            tradecount = row[9]
                        if len(header) == 13:
                            buyTakerAmount = row[9]  # Assuming buyTakerAmount is at index 9
                            buyTakerQuantity = row[10]  # Assuming buyTakerQuantity is at index 10
                            tradecount = row[11]  # Assuming tradecount is at index 11
                            weightAverage = row[12]  # Assuming weightAverage is at index 12

                        # if date is 2021-06-11T12:00:00Z format
                        if len(date.split('T')) == 2:
                            day = date.split('T')[0]
                            hour = date.split('T')[1].split(':')[0]
                            date = day
                        # Create crypto_history_data object
                        if len(date.split(' ')) == 2:
                            day = date.split(' ')[0]
                            time = date.split(' ')[1]
                            # if time is in 00:000:00 format
                            if len(time) == 8:
                                hour = int(time[:2])
                            # if time is  00-AM format
                            if len(time) == 5:
                                hour = time.split('-')[0]
                                # if PM then add 12
                                if time.split('-')[1] == "PM":
                                    hour = int(hour) + 12
                            date = day
                        else:
                            hour = 0

                        crypto_data = CryptoHistoryData(
                            unix_timestamp=unix_timestamp,
                            date=date,
                            hour=hour,
                            symbol=symbol,
                            open_val=open_val,
                            high=high,
                            low=low,
                            close=close,
                            volume_in=volume_in,
                            volume_in_symbol=volume_in_label,
                            volume_out=volume_out,
                            volume_out_symbol=volume_out_label,
                            tradecount=tradecount,
                            provider=provider
                        )
                        # Insert into csv file
                        with open(data_csv_path, 'a', newline='') as data_csv:
                            csv_writer = csv.writer(data_csv)
                            tuple_without_id = crypto_data.toTuple()[1:]
                            # Write the tuple to the CSV file using csv_writer.writerow
                            csv_writer.writerow(tuple_without_id)
                            chunk_count += 1
                            if chunk_count == chunk_size:
                                chunk_count = 0
                                file_count += 1
                                data_csv_path = os.path.join(data_csv_dir_path, f"crypto_history_data_{file_count}.csv")
                        total_progress.update(1)
total_progress.close()

# send by scp to server
print("Sending to server...")
os.system("sudo scp -P 3022 -r ./csv lexit@145.239.146.80:/tmp/")
# insert into database
print("Inserting into database...")
csv_dir = os.listdir(data_csv_dir_path)
# init insert progress bar
insert_progress = tqdm(total=len(csv_dir), desc="Insert Progress", unit="csv")
for csv_file in csv_dir:
    server_path = "/tmp/csv"
    csv_path = os.path.join(server_path, csv_file)
    try :
        db.load_data_file(csv_path)
        insert_progress.update(1)
    except Exception as e:
        print(e)
        print(csv_path)
insert_progress.close()
end_time = datetime.datetime.now()

print("Done! \n")
print(f"Start time: {start_time}")
print(f"End time: {end_time}")
print(f"Total time: {end_time - start_time}")


