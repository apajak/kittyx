import json
import sys
import os
from datetime import datetime

# Function to load a JSON file
def load_json_file(file_path):
    try:
        with open(file_path, 'r') as file:
            data = json.load(file)
        return data
    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")
        sys.exit(1)
    except json.JSONDecodeError:
        print(f"Error: Unable to parse JSON file '{file_path}'.")
        sys.exit(1)

# Function to merge two symbol lists
def merge_symbol_lists(main_list, new_list):
    for symbol in new_list:
        if symbol not in main_list:
            main_list.append(symbol)

# Check if a valid JSON file path is provided as an argument
if len(sys.argv) != 2:
    print("Usage: python add_new_symbols.py <path_to_new_symbols.json>")
    sys.exit(1)

new_symbols_file_path = sys.argv[1]

# Check if the new symbols JSON file exists
if not os.path.exists(new_symbols_file_path):
    print(f"Error: File '{new_symbols_file_path}' not found.")
    sys.exit(1)

# Load the current symbol list
current_symbol_list_path = 'symbol_list.json'
symbol_list = load_json_file(current_symbol_list_path)

# Load the new symbols from the provided JSON file
new_symbols = load_json_file(new_symbols_file_path)

# Merge the new symbols into the current symbol list
merge_symbol_lists(symbol_list, new_symbols)

# Save the updated symbol list
with open(current_symbol_list_path, 'w') as symbol_list_file:
    json.dump(symbol_list, symbol_list_file, indent=4)

print(f"New symbols have been added to '{current_symbol_list_path}'.")
