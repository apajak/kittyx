import requests
import time
import json
from datetime import datetime
from database_manager import DatabaseManager
from dotenv import load_dotenv
import os
from crypto_history_manager import CryptoHistoryData

# database connection
load_dotenv(".env")
db_host = os.getenv("DATABASE_HOST")
db_user = os.getenv("DATABASE_USER")
db_password = os.getenv("DATABASE_PASSWORD")
db_name = os.getenv("DATABASE_NAME")
db = DatabaseManager(db_host, db_user, db_password, db_name)

current_unix_time = int(time.time())
date = time.strftime('%Y-%m-%d', time.localtime(current_unix_time))
print(current_unix_time)
print(date)

root_url = 'https://api.coingecko.com/api/v3/'

# # first try ping endpoint to see if API is up
# ping_url = root_url + 'ping'
# ping_response = requests.get(ping_url)
# if ping_response.status_code == 200:
#     print('API is up')
# else:
#     print('API is down')
#     print(f'ping_response.status_code: {ping_response.status_code}')
#     exit()

# get list of coins
coins_list_url = root_url + 'coins/list'

# coins_list_response = requests.get(coins_list_url)
# if coins_list_response.status_code == 200:
#     print('coins_list_response.status_code: 200')
#     coins_list_json = coins_list_response.json()
# else:
#     print(f'coins_list_response.status_code: {coins_list_response.status_code}')
#     exit()
# for dev purposes, use local file

coins_list_json_file = open('coins_list.json', 'r')
coins_list_json = json.load(coins_list_json_file)

# convert list of coins to dict
coins_dict = json.loads(json.dumps(coins_list_json))
print(len(coins_dict))
# Create a dictionary mapping coin Symbol to coin dictionaries
coin_symbol_map = {coin['symbol']: coin for coin in coins_dict}
symbole = 'btc'

# get coin id
coin_id = coin_symbol_map[symbole]['id']
print(coin_id)

# get ohlc data
def get_ohlc_data(coin_id):
    vs_currency = 'usd'
    day = 1
    precision = 4

    ohlc_url = root_url + f'coins/{coin_id}/ohlc?vs_currency={vs_currency}&days={day}'
    ohlc_response = requests.get(ohlc_url)
    if ohlc_response.status_code == 200:
        print('ohlc_response.status_code: 200')
        ohlc_json = ohlc_response.json()
    else:
        print(f'ohlc_response.status_code: {ohlc_response.status_code}')
        return None
    return ohlc_json

def convert_unix_time_to_date(unix_time):
    unix_time = int(unix_time) / 1000
    return datetime.utcfromtimestamp(unix_time).strftime('%Y-%m-%d %H:%M:%S')

# olhc_json = get_ohlc_data(coin_id)
# # update olhc_gecko.json file with new data
# with open('olhc_gecko.json', 'w') as olhc_json_file:
#     json.dump(olhc_json, olhc_json_file, indent=4)


#  for dev purposes, use local file
olhc_json_file = open('olhc_gecko.json', 'r')
olhc_json = json.load(olhc_json_file)

for olhc in olhc_json:
    print(olhc)
    print(convert_unix_time_to_date(olhc[0]))

for symbol in db.get_crypto_history_by_out_symbol('usd'):
    print(symbol)