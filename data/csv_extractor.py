from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys  # Import Keys to simulate keyboard input
from selenium.webdriver.support.ui import Select
import time
import re
import os
import requests

# Create a WebDriver instance (you must have Chrome or another compatible browser)
driver = webdriver.Chrome()

# URL of the page
url = input("Enter the URL of the page you want to scrape: ")
# Save Directory
dir_name = url.split('/')[-2]
# add maj for first letter
dir_name = dir_name[0].upper() + dir_name[1:]
save_path = f'./data/{dir_name}'
# Create the directory if it doesn't exist
if not os.path.exists(save_path):
    os.makedirs(save_path)

# Open the web page
driver.get(url)

# Wait a few seconds to allow the content to load (you can adjust the delay if needed)
driver.implicitly_wait(10)

# Find and select the option to show 100 rows per page
select = Select(driver.find_element(By.NAME, "available_length"))
select.select_by_value("100")

# Wait for the page to refresh with 100 rows
time.sleep(0.5)  # Adjust the delay if needed

# Find the search input field and enter "USD"
search_input = driver.find_element(By.CSS_SELECTOR, "input[type='search']")
search_input.send_keys("USD")
search_input.send_keys(Keys.RETURN)  # Press Enter to perform the search

# Wait for the search results to load (adjust the delay if needed)
time.sleep(0.5)

# Find the div element containing the information about the number of entries
info_div = driver.find_element(By.CSS_SELECTOR, "div#available_info")
info_text = info_div.text
# Define a regex pattern to match numbers with commas
pattern = r'\d{1,3}(?:,\d{3})*(?=\sentries)'
# Find all matches in the string
matches = re.findall(pattern, info_text)
# Extract the desired number (1,099)
filtered_entries = matches[0]
# Remove commas and convert to an integer
filtered_entries = int(filtered_entries.replace(',', ''))
print(f"Filtered entries: {filtered_entries}")
download_count = 0
# Find the total number of pages
# Find the total number of pages from the last page number in pagination
total_pages_element = driver.find_element(By.CSS_SELECTOR, ".paginate_button:last-child")
total_pages = int(total_pages_element.text)

# Iterate through each page
for page in range(1, total_pages + 1):
    # Find all the rows on the current page
    rows = driver.find_elements(By.CSS_SELECTOR, "table#available tbody tr")

    # Iterate through the rows and extract data
    for row in rows:
        download_count += 1
        scrape_percentage = download_count / filtered_entries * 100
        data = row.find_elements(By.TAG_NAME, "td")
        timeframe = data[1].text
        file_link = data[4].find_element(By.TAG_NAME, "a").get_attribute("href")
        file_name = file_link.split('/')[-1]
        inner_path = os.path.join(save_path, timeframe)
        file_path = os.path.join(inner_path, file_name)
        # Create the directory if it doesn't exist
        if not os.path.exists(inner_path):
            os.makedirs(inner_path)
        # Download the file
        response = requests.get(file_link)
        if response.status_code == 200:
            with open(file_path, 'wb') as file:
                file.write(response.content)
                # Use \r to overwrite the previous print line
            print(f"Downloaded {file_name} to {file_path}, percentage: {scrape_percentage:.2f}%")
        else:
            print(f"Failed to download {file_name}, percentage: {scrape_percentage:.2f}%")

    # If there are more pages, navigate to the next page
    if page < total_pages:
        next_page = driver.find_element(By.CSS_SELECTOR, f".paginate_button[id='available_next']")
        next_page.click()
        time.sleep(0.1)  # Adjust the delay if needed
print()
print("Scraping complete!")



# Close the browser
driver.quit()
